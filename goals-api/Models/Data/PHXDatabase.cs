﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.IO;
using System.Diagnostics;

namespace goals_api.Models
{
    public static class PHXDatabase
    {
        //private static string ConnString = "Data Source=25.95.248.242;Initial Catalog=phoenix_spicers;Persist Security Info=True;User ID=phoenix_user;Password=admin";
        private static string ConnString = "Data Source=192.168.63.11;Initial Catalog=phoenix_test;Persist Security Info=True;User ID=phoenix_user;Password=admin";
        private static Database sql = new Database(ConnString);

        #region Stored Procedure names
        //public const string ImportSalesOrder = "dbo.spPHX_Import_SalesOrder";
        //public const string UpdateSalesOrderCustomer = "dbo."
        #endregion

        public static List<Dictionary<string, object>> GetGoalConfigs(string goalType, string WorkCenter, string InventoryType)
        {
            if ((goalType == null) || (goalType == ""))
                goalType = "";

            if ((WorkCenter == null) || (WorkCenter == "") || (WorkCenter == "ALL"))
                WorkCenter = "";

            if ((InventoryType == null) || (InventoryType == "") || (InventoryType == "ALL"))
                InventoryType = "";


            string qry = "SELECT g.[goal_id], " +
                        "g.[goal_type_id], " +
                        "g.[work_center_id], " +
                        "g.[inventory_type_id], " +
                        "wc.work_center_name, " +
                        "it.inventory_type, " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_0]) [color_0], " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_1]) [color_1], " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_2]) [color_2], " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_3]) [color_3], " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_4]) [color_4], " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_5]) [color_5], " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_6]) [color_6], " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_7]) [color_7], " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_8]) [color_8], " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_9]) [color_9], " +
                        "dbo.[ufn_FormatSecondsToString](g.[color_10]) [color_10], " +
                        "g.[target_output] " +
                "FROM sf_goals g " +
                    "    JOIN sf_goal_types gt ON g.goal_type_id = gt.goal_type_id " +
                        "JOIN sf_work_centers wc ON g.work_center_id = wc.work_center_id " +
                        "JOIN ic_inventory_types it ON g.inventory_type_id = it.inventory_type_id " +
                "WHERE gt.goal_type = '" + goalType + "' " +
                        "AND wc.work_center_name like '%" + WorkCenter + "%' " +
                        "AND it.inventory_type like '%" + InventoryType + "%' " +
                        "AND g.deleted_by is NULL " +
                "ORDER BY wc.work_center_name, " +
                            "it.inventory_type, " +
                            "g.goal_id " +
                            ";";

            return sql.ExecuteQueryToList(qry);
        }

        public static List<Dictionary<string, object>> GetWorkCenters()
        {
            string qry = "SELECT work_center_id, work_center_name " +
                        "FROM sf_work_centers " +
                        "WHERE is_active = 1 " +
                        "ORDER BY work_center_name ASC " +
                                        ";";

            return sql.ExecuteQueryToList(qry);
        }

        public static List<Dictionary<string, object>> GetInventoryTypes()
        {
            string qry = "SELECT inventory_type_id, inventory_type " +
                "FROM ic_inventory_types " +
                "WHERE is_active = 1 " +
                "ORDER BY inventory_type " +
                ";";

            return sql.ExecuteQueryToList(qry);
        }


        public static List<Dictionary<string, object>> AddGoalConfig(string goalType, string workCenterID, string inventoryTypeID, string color0, string color1, string color2, string color3, string color4, string color5, string color6, string color7, string color8, string color9, string color10, string targetOutput, string userID)
        {
            //NULL Checks
            if (color0 == null || color0 == "")
                color0 = "NULL";
            if (color1 == null || color1 == "")
                color1 = "NULL";
            if (color2 == null || color2 == "")
                color2 = "NULL";
            if (color3 == null || color3 == "")
                color3 = "NULL";
            if (color4 == null || color4 == "")
                color4 = "NULL";
            if (color5 == null || color5 == "")
                color5 = "NULL";
            if (color6 == null || color6 == "")
                color6 = "NULL";
            if (color7 == null || color7 == "")
                color7 = "NULL";
            if (color8 == null || color8 == "")
                color8 = "NULL";
            if (color9 == null || color9 == "")
                color9 = "NULL";
            if (color10 == null || color10 == "")
                color10 = "NULL";
            if (targetOutput == null || targetOutput == "")
                targetOutput = "NULL";

            string qry = "INSERT INTO sf_goals" +
                "( " +
                "[goal_type_id], [work_center_id], [inventory_type_id], [color_0], [color_1], [color_2], [color_3], [color_4], [color_5], [color_6], [color_7], [color_8], [color_9], [color_10], [target_output], [created_by], [created_date], [last_modified_by], [last_modified_date] " +
                ") " +
                "SELECT " +
                "(SELECT TOP 1 goal_type_id from sf_goal_types WHERE goal_type = '" + goalType + "'), " +
                workCenterID + ", " + inventoryTypeID + ", " +
                color0 + ", " + color1 + ", " + color2 + ", " + color3 + ", " + color4 + ", " + color5 + ", " + color6 + ", " + color7 + ", " + color8 + ", " + color9 + ", " + color10 + ", " +
                targetOutput + ", " +
                userID + ", '" + DateTime.Now + "', " + //Created By
                userID + ", '" + DateTime.Now + "' " + //Updated By
                "; ";

            return sql.ExecuteQueryToList(qry);
        }

        public static List<Dictionary<string, object>> UpdateGoalConfig(string goalID, string workCenterID, string inventoryTypeID, string color0, string color1, string color2, string color3, string color4, string color5, string color6, string color7, string color8, string color9, string color10, string targetOutput, string userID)
        {
            //NULL Checks
            if (color0 == null || color0 == "")
                color0 = "NULL";
            if (color1 == null || color1 == "")
                color1 = "NULL";
            if (color2 == null || color2 == "")
                color2 = "NULL";
            if (color3 == null || color3 == "")
                color3 = "NULL";
            if (color4 == null || color4 == "")
                color4 = "NULL";
            if (color5 == null || color5 == "")
                color5 = "NULL";
            if (color6 == null || color6 == "")
                color6 = "NULL";
            if (color7 == null || color7 == "")
                color7 = "NULL";
            if (color8 == null || color8 == "")
                color8 = "NULL";
            if (color9 == null || color9 == "")
                color9 = "NULL";
            if (color10 == null || color10 == "")
                color10 = "NULL";
            if (targetOutput == null || targetOutput == "")
                targetOutput = "NULL";

            string qry = "UPDATE sf_goals " +
                "SET " +
                "work_center_id = " + workCenterID + ", " +
                "inventory_type_id = " + inventoryTypeID + ", " +
                "color_0 = " + color0 + ", " +
                "color_1 = " + color1 + ", " +
                "color_2 = " + color2 + ", " +
                "color_3 = " + color3 + ", " +
                "color_4 = " + color4 + ", " +
                "color_5 = " + color5 + ", " +
                "color_6 = " + color6 + ", " +
                "color_7 = " + color7 + ", " +
                "color_8 = " + color8 + ", " +
                "color_9 = " + color9 + ", " +
                "color_10 = " + color10 + ", " +
                "target_output = " + targetOutput + ", " +
                "last_modified_by = " + userID + ", " +
                "last_modified_date = '" + DateTime.Now + "' " +
                "WHERE goal_id = " + goalID;

            return sql.ExecuteQueryToList(qry);
        }

        public static List<Dictionary<string, object>> DeleteGoalConfig(string goalID, string userID)
        {
            string qry = "UPDATE sf_goals " +
                "SET " +
                "last_modified_by = " + userID + ", " +
                "last_modified_date = '" + DateTime.Now + "', " +
                "deleted_by = " + userID + ", " +
                "deleted_date = '" + DateTime.Now + "' " +
                "WHERE goal_id = " + goalID;

            return sql.ExecuteQueryToList(qry);
        }
    }
}