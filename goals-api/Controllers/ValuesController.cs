﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace goals_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values/getgoalconfigs
        [HttpPost, Route("getgoalconfigs")]
        public ActionResult GetGoalConfigs([FromBody] GetGoalConfigsParam value)
        {
             return new JsonResult(Models.PHXDatabase.GetGoalConfigs(value.GoalType, value.WorkCenter, value.InventoryType));
        }

        // POST api/values/getworkcenters
        [HttpPost, Route("getworkcenters")]
        public ActionResult GetWorkCenters()
        { 
            return new JsonResult(Models.PHXDatabase.GetWorkCenters());
        }

        // POST api/values/getinventorytypes
        [HttpPost, Route("getinventorytypes")]
        public ActionResult GetInventoryTypes()
        {
            return new JsonResult(Models.PHXDatabase.GetInventoryTypes());
        }

        // POST api/values/addgoalconfig
        [HttpPost, Route("addgoalconfig")]
        public ActionResult AddGoalConfig([FromBody] AddGoalConfigParam value)
        {
            return new JsonResult(Models.PHXDatabase.AddGoalConfig(value.GoalType, value.WorkCenter, value.InventoryType, value.Color0, value.Color1, value.Color2, value.Color3, value.Color4, value.Color5, value.Color6, value.Color7, value.Color8, value.Color9, value.Color10, value.TargetOutput, value.UserID));
        }

        // POST api/values/updategoalconfig
        [HttpPost, Route("updategoalconfig")]
        public ActionResult UpdateGoalConfig([FromBody] UpdateGoalConfigParam value)
        {
            return new JsonResult(Models.PHXDatabase.UpdateGoalConfig(value.GoalID, value.WorkCenter, value.InventoryType, value.Color0, value.Color1, value.Color2, value.Color3, value.Color4, value.Color5, value.Color6, value.Color7, value.Color8, value.Color9, value.Color10, value.TargetOutput, value.UserID));
        }

        // POST api/values/deletegoalconfig
        [HttpPost, Route("deletegoalconfig")]
        public ActionResult DeleteGoalConfig([FromBody] UpdateGoalConfigParam value)
        {
            return new JsonResult(Models.PHXDatabase.DeleteGoalConfig(value.GoalID, value.UserID));
        }
    }

    public class Param
    {
        public string Value { get; set; }
    }

    public class GetGoalConfigsParam
    {
        public string GoalType { get; set; }
        public string WorkCenter { get; set; }
        public string InventoryType { get; set; }
    }

    public class AddGoalConfigParam
    {
        public string GoalType { get; set; }
        public string WorkCenter { get; set; }
        public string InventoryType { get; set; }
        public string Color0 { get; set; }
        public string Color1 { get; set; }
        public string Color2 { get; set; }
        public string Color3 { get; set; }
        public string Color4 { get; set; }
        public string Color5 { get; set; }
        public string Color6 { get; set; }
        public string Color7 { get; set; }
        public string Color8 { get; set; }
        public string Color9 { get; set; }
        public string Color10 { get; set; }
        public string TargetOutput { get; set; }
        public string UserID { get; set; }
    }

    public class UpdateGoalConfigParam : AddGoalConfigParam
    {
        public string GoalID { get; set; }
    }
}
